# 第2章 深入理解--koa2+异步编程模型

* Koa非常的精简，基本上，没有经过二次开发的Koa根本“不能”用。本章讲解Koa的重要特性，理解什么是洋葱模型？以及在KOA中如何进行异步编程？

## 1 软件与环境

## 2 node一小步，前端一大步

## 3 koa 的精简特性与二次开发必要性分析

## 4 模块加载、ES、TS、Babel 浅析

## 5 koa 的中间件

## 6 洋葱模型

## 7 强制 Promise

## 8 深入理解 async 和 await

## 9 为什么一定要保证洋葱模型？
