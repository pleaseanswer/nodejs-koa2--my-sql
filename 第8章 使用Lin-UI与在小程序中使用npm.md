# 第8章 使用Lin-UI与在小程序中使用npm

* 如何使用小程序openid构建用户系统，如何从小程序中携带JWT令牌。

## 1 Lin-UI 组件库安装

## 2 在小程序中登录

## 3 Token Verify 接口

## 4 数据库设计的好思路（实体表与业务表）

## 5 Music、Sentence、Movie 模型定义

## 6 Flow 模型与导入 SQL 数据

## 7 在小程序中携带令牌

## 8 Sequelize 模型的序列化
