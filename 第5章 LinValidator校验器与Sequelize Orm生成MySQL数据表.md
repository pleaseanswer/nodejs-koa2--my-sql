# 第5章 LinValidator校验器与Sequelize Orm生成MySQL数据表

## 1 Lin-Validator使用指南

## 2 Lin-Validator获取HTTP参数

## 3 配置文件与在终端显示异常

## 4 关系型数据库与非关系型数据库

## 5 Navicat 管理 MySQL

## 6 Sequelize 初始化配置与注意事项

## 7 User 模型与用户唯一标识设计探讨

## 8 Sequelize 个性化配置与数据维护策略

## 9 LinValidator 综合应用
